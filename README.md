Email Crawler
-------------

Implement a program that crawls the web and collects e-mail addresses from the pages it visits.
* The program reads the address of the first webpage from the command line parameter.
* The program finds and persists all email addresses it finds on the page (any method for persisting is okay).
* The program finds all links on the webpage, and opens these links one-by-one. 
With each page loaded, it acts the same as with the first page (loads links and emails).
* The program does not visit any web page twice, 
an exception being the first address which is given as the parameter - this should be visited anyways.
* While running, the program prints/displays its current state, 
i.e. prints the address of the web page that is currently being processed and notifies when an email is found.
* On each start, the program prints out/displays the e-mail addresses that have been persisted.

You are free to choose any language/tools/libraries you like.

If using Java for implementing the program, Jsoup (https://jsoup.org) and 
H2 database (http://www.h2database.com) might become handy.

Dependencies must be managed with a package manager (Maven, Gradle, npm, pip etc).

Writing any test for any part of the program is not a must, but is definitely a bonus.
