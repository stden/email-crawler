package crawler;

import crawler.db.DbConnector;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.SQLException;

@DisplayName("Clear database + run")
class FunctionalTest {
    @BeforeAll
    @DisplayName("Creating database structure")
    static void createDatabaseStructure() throws SQLException, IOException, ClassNotFoundException {
        DbConnector.initDatabase("DB.SQL");
    }

    @Test
    @DisplayName("Crawls real web-page")
    void testRealSite() throws SQLException, ClassNotFoundException, IOException {
        DbConnector.clearData();

        Main.main("https://en.wikipedia.org");
    }
}

