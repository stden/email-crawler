package crawler.web;

import crawler.db.DbConnector;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

class WebPageScannerTest {
    @BeforeAll
    static void setUpAll() throws SQLException, IOException, ClassNotFoundException {
        DbConnector.initDatabase("DB.SQL");
    }

    @Test
    @DisplayName("Get all links")
    void testGetAllLinks() throws IOException, SQLException, ClassNotFoundException {
        DbConnector.clearData();
        WebPageScanner scanner = new WebPageScanner("https://en.wikipedia.org");
        assertTrue(scanner.getNewLinks().size() > 1);
        scanner.getNewLinks().forEach(System.out::println);
    }
}
