package crawler;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static crawler.web.WebPageScanner.normalize;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * https://en.wikipedia.org/wiki/URL_normalization
 */
@DisplayName("URL normalization")
class URLNormalizationTest {
    @Test
    @DisplayName("Removes leading and trailing white spaces in the URL")
    void testRemoveWhitespaces() {
        assertEquals("http://www.example.com/",
                normalize("", "     http://www.example.com/     "));
    }

    @Test
    @DisplayName("Converting the scheme and host to lower case")
    void testNormalizeLowerCase() {
        assertEquals("http://www.example.com/",
                normalize("", "HTTP://www.Example.com/"));
    }

    @Test
    @DisplayName("Capitalizing letters in escape sequences")
    void testNormalize() {
        assertEquals("http://www.example.com/a%C2%B1b",
                normalize("", "http://www.example.com/a%c2%b1b"));
    }

    @Test
    @DisplayName("Decoding percent-encoded octets of unreserved characters")
    void testDecodingPercentEncodedOctets() {
        assertEquals("http://www.example.com/~username/",
                normalize("", "http://www.example.com/%7Eusername/"));
    }

    @Test
    @DisplayName("Removing the default port")
    void testRemovingDefaultPort() {
        assertEquals("http://www.example.com/bar.html",
                normalize("", "http://www.example.com:80/bar.html"));
    }

    @Test
    @DisplayName("Removing dot-segments")
    void testRemovingDotSegments() {
        assertEquals("http://www.example.com/a/c/d.html",
                normalize("", "http://www.example.com/../a/b/../c/./d.html"));
    }

    @Test
    @DisplayName("Removing the fragment")
    void testRemovingTheFragment() {
        assertEquals("http://www.example.com/bar.html",
                normalize("", "http://www.example.com/bar.html#section1"));
    }

    @Test
    @DisplayName("Removing duplicate slashes")
    void testRemovingDuplicateSlashes() {
        assertEquals("http://www.example.com/foo/bar.html",
                normalize("", "http://www.example.com/foo//bar.html"));
    }

    @Test
    @DisplayName("Use same protocol")
    void testUseSameProtocol() {
        assertEquals("https://shop.wikimedia.org/",
                normalize("https://en.wikipedia.org", "//shop.wikimedia.org"));
    }

    @Test
    @DisplayName("Relative links")
    void testRelativeLinks() {
        assertEquals("https://en.wikipedia.org/wiki/Wikipedia:About",
                normalize("https://en.wikipedia.org", "/wiki/Wikipedia:About"));
    }
}

