package crawler.db;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Persistence: work with database")
class DbConnectorTest {
    @BeforeAll
    @DisplayName("Creating database structure")
    static void createDatabaseStructure() throws SQLException, IOException, ClassNotFoundException {
        DbConnector.initDatabase("DB.SQL");
    }

    @Test
    @DisplayName("Mark in the database that URL is visited")
    void testMarkUrlVisited() throws SQLException, ClassNotFoundException {
        DbConnector.clearData();

        String url = "https://github.com";
        assertFalse(DbConnector.urlVisited(url), "URL not yet visited");

        DbConnector.addUrlVisited(url);
        assertTrue(DbConnector.urlVisited(url), "URL should be visited");
    }

    @Test
    @DisplayName("Add e-mail to database")
    void testAddEmail() throws SQLException, ClassNotFoundException {
        DbConnector.clearData();

        String email = "my-email@example.com";
        assertFalse(DbConnector.emailExists(email));
        assertIterableEquals(Collections.emptyList(), DbConnector.getEmails());

        assertTrue(DbConnector.addEmail(email));
        assertTrue(DbConnector.emailExists(email));
        assertIterableEquals(Collections.singletonList(email), DbConnector.getEmails());

        assertFalse(DbConnector.addEmail(email));
        assertTrue(DbConnector.emailExists(email));
        assertIterableEquals(Collections.singletonList(email), DbConnector.getEmails());
    }

    @Test
    @DisplayName("Add many e-mails to database")
    void testManyEmails() throws SQLException, ClassNotFoundException {
        DbConnector.clearData();

        for (int i = 1; i <= 1000; i++) {
            String email = "email-" + i + "@example.com";
            assertFalse(DbConnector.emailExists(email));
            assertTrue(DbConnector.addEmail(email));
            assertTrue(DbConnector.emailExists(email));

            assertEquals(i, DbConnector.getEmails().size());
        }
    }
}

