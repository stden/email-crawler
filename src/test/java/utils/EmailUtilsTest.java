package utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.EmailUtils.extractEmails;

@DisplayName("Email tests")
class EmailUtilsTest {
    private static Set<String> setOf(String... strings) {
        return new HashSet<>(Arrays.asList(strings));
    }

    @Test
    @DisplayName("Extract all emails from text")
    void testExtractEmails() {
        assertAll(
                () -> assertEquals(setOf("test@example.com"), extractEmails("   test@example.com ")),
                () -> assertEquals(setOf("a@x.eu", "b@y.eu"), extractEmails("<tag>  a@x.eu b@y.eu </tag>")),
                () -> assertEquals(setOf("me.100.1@x.ee"), extractEmails("  me.100.1@x.ee  me@%*.com   me@me@me "))
        );
    }
}
