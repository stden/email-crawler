package utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {
    /**
     * Read whole file into a string
     *
     * @param fileName file name
     * @return file contents as one string
     * @throws IOException reading errors
     */
    public static String fileToStr(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);
    }
}
