package crawler.web;

import crawler.db.DbConnector;
import crawlercommons.filters.basic.BasicURLNormalizer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.EmailUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class WebPageScanner {
    private static final Logger log = LogManager.getLogger(WebPageScanner.class);

    private static final BasicURLNormalizer urlNormalizer = new BasicURLNormalizer();
    private static final Set<String> knownEmails = new HashSet<>();

    private static final Set<String> knownLinks = new HashSet<>();

    static {
        try {
            knownLinks.addAll(DbConnector.getVisitedUrls());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private final Set<String> newLinks = new HashSet<>();

    public WebPageScanner(String url) throws IOException, SQLException, ClassNotFoundException {
        log.info("Load " + url);
        DbConnector.addUrlVisited(url);

        Document doc = Jsoup.connect(url)
                // .proxy("proxy", 80) // sets a HTTP proxy
                .get();

        for (String email : EmailUtils.extractEmails(doc.text())) {
            if (knownEmails.add(email) && DbConnector.addEmail(email)) {
                log.info("E-mail: " + email);
            }
        }

        Elements elements = doc.select("a[href]");
        for (Element e : elements) {
            String href = normalize(url, e.attr("href"));
            if (href != null && !href.isEmpty()) {
                if (knownLinks.add(href)) {
                    log.info(href);
                    if (!DbConnector.urlVisited(href)) {
                        newLinks.add(href);
                    }
                }
            }
        }
    }

    public static String normalize(final String url, String link) {
        if (link.startsWith("//")) {
            try {
                return urlNormalizer.filter(new URI(url).getScheme() + ":" + link);
            } catch (URISyntaxException e) {
                log.error(url + " " + link, e);
                return null;
            }
        }
        if (link.startsWith("/")) {
            return urlNormalizer.filter(url + link);
        }
        return urlNormalizer.filter(link);
    }

    public Set<String> getNewLinks() {
        return newLinks;
    }
}
