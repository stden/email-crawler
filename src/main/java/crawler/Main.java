package crawler;

import crawler.db.DbConnector;
import crawler.web.WebPageScanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main {
    private static final int WEB_PAGES_LIMIT = 10000;

    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(final String... args) throws SQLException, ClassNotFoundException, IOException {
        DbConnector.initDatabase("DB.SQL");

        if (args.length != 1) {
            log.info("Run: Main <StartWebPage>");
            showPersistedEmails();
            return;
        }
        showPersistedEmails();

        // Reads first WebPage address from the command line parameter
        Queue<String> webPagesQueue = new LinkedList<>();
        webPagesQueue.add(args[0]);

        int webPagesCount = 0;
        while (!webPagesQueue.isEmpty()) {
            webPagesCount++;
            if (webPagesCount > WEB_PAGES_LIMIT) {
                return;
            }

            String url = webPagesQueue.remove();
            WebPageScanner scanner = new WebPageScanner(url);
            DbConnector.addUrlVisited(url);

            for (String link : scanner.getNewLinks()) {
                if (!DbConnector.urlVisited(link)) {
                    webPagesQueue.add(link);
                }
            }
        }
    }

    // On each start, the program prints out/displays the e-mail addresses that have been persisted
    private static void showPersistedEmails() throws SQLException, ClassNotFoundException {
        log.info("Persisted emails:");
        List<String> emails = DbConnector.getEmails();
        if (emails.size() == 0) {
            log.info("No e-mails");
        } else {
            emails.forEach(log::info);
        }
    }
}