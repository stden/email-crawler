package crawler.db;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import utils.FileUtils;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.apache.logging.log4j.core.util.Loader.getClassLoader;

public class DbConnector {
    private static DataSource dataSource;

    private static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (dataSource == null) {
            Class.forName("org.h2.Driver", true, getClassLoader());

            HikariConfig config = new HikariConfig("/hikari.properties");
            //config.setMaximumPoolSize(100);
            //config.setDataSourceClassName("org.h2.jdbcx.JdbcDataSource");
            //config.setConnectionTestQuery("VALUES 1");
            //config.addDataSourceProperty("URL", "jdbc:h2:~/emails");
            //config.addDataSourceProperty("user", "sa");
            //config.addDataSourceProperty("password", "");

            HikariDataSource hikariDataSource = new HikariDataSource(config);
            hikariDataSource.setConnectionTimeout(800);
            dataSource = hikariDataSource;
        }
        return dataSource.getConnection();
    }

    public static void clearData() throws SQLException, ClassNotFoundException {
        executeQuery("DELETE FROM URL_VISITED");
        executeQuery("DELETE FROM EMAIL");
    }

    private static void executeQuery(String sql) throws SQLException, ClassNotFoundException {
        try (Connection con = getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(sql)) {
            preparedStatement.executeUpdate();
        }
    }

    public static void initDatabase(String sqlFileName) throws SQLException, ClassNotFoundException, IOException {
        try (Connection con = getConnection()) {
            String sqlScript = FileUtils.fileToStr(sqlFileName);
            for (String sql : sqlScript.split(";")) {
                if (!sql.trim().isEmpty()) {
                    try (Statement stmt = con.createStatement()) {
                        stmt.execute(sql);
                    }
                }
            }
        }
    }

    public static void addUrlVisited(String url) throws SQLException, ClassNotFoundException {
        String insertSQL = "MERGE INTO URL_VISITED(URL) VALUES (?)";
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(insertSQL)) {
            ps.setString(1, url);
            ps.executeUpdate();
        }
    }

    public static boolean urlVisited(String url) throws SQLException, ClassNotFoundException {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM URL_VISITED WHERE URL = ?")) {
            ps.setString(1, url);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return true;
                }
            }
        }
        return false;
    }

    static boolean emailExists(String email) throws SQLException, ClassNotFoundException {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement("SELECT * FROM EMAIL WHERE EMAIL = ?")) {
            ps.setString(1, email);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return true;
                }
            }
        }
        return false;
    }

    private static void addEmailIfNotExists(String email) throws SQLException, ClassNotFoundException {
        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement("MERGE INTO EMAIL(EMAIL) VALUES(?)")) {
            ps.setString(1, email);
            ps.executeUpdate();
        }
    }

    public static List<String> getEmails() throws SQLException, ClassNotFoundException {
        List<String> emails = new ArrayList<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery("SELECT EMAIL FROM EMAIL ORDER BY EMAIL")) {
            while (rs.next()) {
                emails.add(rs.getString("EMAIL"));
            }
        }
        return emails;
    }

    public static Set<String> getVisitedUrls() throws SQLException, ClassNotFoundException {
        Set<String> urls = new HashSet<>();
        try (Connection connection = getConnection();
             Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery("SELECT URL FROM URL_VISITED")) {
            while (rs.next()) {
                urls.add(rs.getString("URL"));
            }
        }
        return urls;
    }

    public static boolean addEmail(String email) throws SQLException, ClassNotFoundException {
        if (DbConnector.emailExists(email)) {
            return false;
        }
        addEmailIfNotExists(email);
        return true;
    }
}
